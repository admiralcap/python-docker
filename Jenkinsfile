/**

This pipeline calls docker directly and uses a script{} section to check the
output, which I originally did so that I could run the check from OUTSIDE of
the container. But then in order to run it on a docker agent I ended up having
to curl on the container anyway. So it defeats the purpose open the possibility
(although I've tried to account for it) that the container will be left
running.

Might be better to go back to the docker{} blocks instead.

Last working commit is:
commit 565cb0ffd5bf883609285cebf939e85e033fe997

**/

pipeline {
	//agent { 
	//	label 'worker'
	//}
	agent any
	environment {
		GITLAB_CREDS = credentials('gitlab-registry-python-docker')
		GITLAB_REGISTRY_URL = 'https://registry.gitlab.com'
		GITLAB_REPO = 'registry.gitlab.com/admiralcap/python-docker'
		DOCKER_TAG = "J${BUILD_NUMBER}_${currentBuild.startTimeInMillis}"
	}
	stages {
		stage('Build') {
			steps {
				sh """
					docker build -t ${GITLAB_REPO}:${DOCKER_TAG} .
				"""
			}
		}
		stage('Run forest run!') {
			steps {
				sh "docker run -d --rm --name pd ${GITLAB_REPO}:${DOCKER_TAG}"
			}
		}
		stage('Test and rm') {
			steps {
				
				script {
					echo "Hello from script section!"
					try {
						curl_out = sh (
							script: 'docker exec -i pd curl --silent http://localhost:5000',
							returnStdout: true
						).trim()
						if ("${curl_out}" == 'Hello, Docker!') {
							echo "Check passed!"
						}
						else {
							echo "Check failed! Something is amis!"
							echo "But DO NOT abort the pipeline now. It still needs to stop the container!"
							currentBuild.result = 'FAILURE'
						}
					}
					catch(err) {
						echo "Check failed! Something is amis!"
						echo err.getMessage()
						currentBuild.result = 'FAILURE'
					}

				}

				sh 'docker stop --time 1 pd'
			}
		}
		stage('Push image') {
			when {
				not {
					expression {
						equals(actual: currentBuild.result, expected: "FAILURE")
					}
				}
			}
			steps {
				echo "Push it real good"
				//withDockerRegistry([ credentialsId: "gitlab-registry-python-docker", url: "https://registry.gitlab.com" ]) {
        //  sh  "docker push ${GITLAB_REPO}:${DOCKER_TAG}" 
        //}

				/**
					NOTE: The above works using withDockerRegistry(), but I'm doing it
					manually to test out the credentials() function used in the
					environment{} section.
				**/

				sh 'echo $GITLAB_CREDS_PSW | docker login -u $GITLAB_CREDS_USR --password-stdin $GITLAB_REGISTRY_URL'
				sh "docker tag ${GITLAB_REPO}:${DOCKER_TAG} ${GITLAB_REPO}:latest"
				sh "docker push ${GITLAB_REPO}:${DOCKER_TAG}"
				sh "docker push ${GITLAB_REPO}:latest"
			}
		}
	}
	post {
		always {
			sh 'docker logout $GITLAB_REGISTRY_URL'
			// clean up the local image
			sh "docker image rm --force ${GITLAB_REPO}:${DOCKER_TAG}"
			sh "docker image rm --force ${GITLAB_REPO}:latest"
		}
	}
}
