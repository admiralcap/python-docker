#FROM python:3.11.4-bookworm
FROM python:3.11.4-slim-bookworm

RUN apt update && apt install -y curl

WORKDIR /python-docker

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]

